---
type: event
path: /event/makerscafe-6
title: 'Makerscafé #6'
date: 2019-10-01
tags: ["Events","Haagse Makers","makerscafe","permanent future lab"]
start: 2015-12-03 19:00:00 
end: 2015-12-03 22:00:00 
event_url: http://www.meetup.com/haagsemakers/events/226002665/ 
organizers: ["haagse_makers"]
locations: ["permanent_future_lab_muscom"]
featuredImage: makerscafe.png
---

**Do you create things? Do you like to exchange knowledge, get to know awesome projects & show what you are working on? -> See you on the first Thursday of the month!** It will be an epic collaboration between [Permanent Future Lab](https://permanentfuturelab.wiki/wiki/Permanent_Future_Lab_Wiki)and [Haagse Makers](http://haagsemakers.nl/)! This time the Maker Cafe is hosted by the Permanent Future Lab at the [Museum of Communication](http://www.muscom.nl/). The full agenda is TBA but the general schedule is: • **Talks: Maker's Show and Tell** (5 - 10 minutes each) **• 'Hack' corner for experimenting and prototyping**

Register at meetup.com: [http://www.meetup.com/haagsemakers/events/226002665/](http://www.meetup.com/haagsemakers/events/226002665/)
------------------------------------------------------------------------------------------------------------------------------------