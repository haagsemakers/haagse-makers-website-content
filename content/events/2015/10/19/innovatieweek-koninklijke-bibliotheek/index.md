---
type: event
path: /event/innovatieweek-koninklijke-bibliotheek
title: 'Innovatieweek Koninklijke Bibliotheek'
date: 2019-10-01
tags: ["bibliotheek"]
start: 2015-10-19 08:00:00 
end: 2015-10-23 17:00:00 
event_url: https://www.kb.nl/ob/algemene-programmas/innovatieagenda/innovatieweek-kb 
organizers: ["koninklijke_bibliotheek"]
locations: ["koninklijke_bibliotheek"]
featuredImage: kb-innovatieweek-label-rgb.png
---

Innovatieweek: 19-23 oktober 2015
---------------------------------

Innovatie bij (openbare) bibliotheken stimuleren en ondersteunen: dat is het doel van de Koninklijke Bibliotheek tijdens haar Innovatieweek. Met allerlei kleinschalige sessies, zoals workshops, wordt het een ‘doe-week’ rond het thema 'bibliotheekinnovatie'.

Meer info: [https://www.kb.nl/ob/algemene-programmas/innovatieagenda/innovatieweek-kb](https://www.kb.nl/ob/algemene-programmas/innovatieagenda/innovatieweek-kb)