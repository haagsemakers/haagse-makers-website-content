---
type: event
path: /event/maker-education-the-hague-session
title: 'Haagse Makers Border Sessions: Make-Learn-Invent at the Makerskwartier'
date: 2019-10-01
tags: ["boho","Border Sessions","Events","Haagse Makers","makered","makerskwartier"]
start: 2015-11-12 16:00:00 
end: 2015-11-12 17:00:00 
event_url: www.bordersessions.org/ignite-the-maker-education-community-the-hague/ 
organizers: ["haagse_makers"]
locations: ["theater_aan_het_spui"]
featuredImage: boho.png
---

**Let's ignite the Maker Community The Hague**

During Border Sessions 2016 we organise a power session about Maker Education (MakerEd) and the plans for the Maker District / Makerskwartier in The Hague. We are interested in bringing hands-on creative projects (a.k.a making) and the Maker Mindset into education and society. With a growing interest in the maker community and events like Maker Fairs, our goal is to bring all those interested in DIY (Do It Yourself) and DIT (Do It Together) practices together to share ideas and collaborate on projects.

Haagse Makers has initiated the Maker District The Hague Concept (Makerskwartier): a living and working location in the old town of The Hague, in walking distance of the main shopping area, the multi-cultural Schilderwijk and Stationsbuurt. For and by makers in design, art, culture, film, food, tech, architecture and urban planning. Innovators in their expertise and who want to use their skills to improve themselves, BOHO and the city in an innovative way. 

This session is intended for educators, makers and city makers who would like to work together on stimulating the maker movement in The Hague.