---
type: event
path: /event/tuinbouwreststromen-en-insectenkweek
title: Tuinbouwreststromen en insectenkweek
date: 2019-10-01
tags: ["food"]
start: 2019-10-10 13:00:00
end: 2019-10-10 17:00:00
event_url: https://greenportwestholland.nl/seminar-greenport-en-impactcity-over-kansen-tuinbouwafval-voor-insectenkweek/
organizers: ["greenport_west_holland", "impact_city"]
venue: apollo14
featuredImage: Twitter-banner-insectenkweek-03.jpg
---

Insecten zijn de voeding van de toekomst. Dat biedt mogelijkheden voor de tuinbouw. Immers, als groeimedium voor bijvoorbeeld krekels en vliegen zijn sommige tuinbouwreststromen prima geschikt. Daarover organiseren Biobased Greenport West-Holland en ImpactCity op 10 oktober een mini-symposium in Apollo 14 in Den Haag. Deze bijeenkomst is tevens programmaonderdeel van Dutch Food Week.

De biobased economie biedt grote kansen voor de tuinbouw. Zo kunnen tuinbouwrestmaterialen op verschillende manieren verwaard worden. Voorbeelden daarvan zijn het gebruik van tomatenstengels voor de productie van papier of textiel. Welke eisen worden voor dergelijke toepassingen gesteld aan die restmaterialen? Hoe organiseren we een marktsysteem? En hoe kunnen ondernemers daarop inspelen?

Daarover gaan de themaweken die Biobased Greenport West-Holland in samenwerking met Bioboost najaar 2019 organiseert. Er zijn dit jaar drie van dergelijke themaweken: ‘De week van tuinbouwreststromen en biobased bouwmaterialen’ vond in september plaats, van 7 tot en met 16 oktober is ‘De week van tuinbouwreststromen en insectenkweek’ en van 9 tot en met 13 december is ‘De week van tuinbouwreststromen en papier en textiel’.

CricketOne, Sustainable Protein, Hogeschool Vives en De Krekerij

Op donderdagmiddag 10 oktober organiseren Biobased Greenport West-Holland, Bioboost en ImpactCity een mini-symposium over de mogelijkheden van insectenkweek op basis van tuinbouwreststromen. Deze bijeenkomst vindt plaats in Apollo 14, het ImpactCity verzamelgebouw voor innovatieve bedrijven op het gebied van onder meer vrede, recht, veiligheid, energie, gezondheid, voedsel, water en participatie. ImpactCity is het Haagse ecosysteem waar alles draait om doing good and doing business. Innovaties voor een betere wereld krijgen hier de ruimte en ondersteuning die zij nodig hebben om van de grond te komen. Wethouder Saskia Bruines van de gemeente Den Haag opent namens ImpactCity het mini-symposium.

Eén van de sprekers is Bicky Nguyen van CricketOne, het Vietnamese bedrijf dat ook een kantoor heeft in Den Haag. CricketOne is leverancier van voedselingrediënten voor zowel basisvoeding als sport- en klinische voeding. Daarvoor extraheert het eiwitten uit krekels. CricketOne gebruikt in de productielocatie in Vietnam onder meer intensieve veredelingstechnieken, verticale landbouw en Internet of Things-controlesystemen en werkt nauw samen met lokale boeren.

Sustainable Protein ontwikkelt, bouwt en exploiteert proteïnebedrijven in de Benelux. Door middel van een systematische geïndustrialiseerde bio-conversie proces wordt bestaande afval van organische voedingsstoffen gerecycled en getransformeerd door de larven van de zwarte soldaatvlieg in natuurlijke en hoogwaardige eiwitten. Deze eiwitten worden verkocht voor gebruik in dier- en visvoeding. Directeur Bart Nollen zal tijdens het mini-symposium vertellen over de activiteiten en ambities van Sustainable Proteine.

Hogeschool Vives (in het West-Vlaamse Ghent) is één van de partners van BioBoost, de samenwerking tussen een aantal Nederlandse, Belgische en Britse regio’s op het gebied van biobased tuinbouw. Thomas Spranghers doet op Hogeschool Vives onderzoek naar grootschalige kweek van insecten, zoals de zwarte soldaatvlieg. Tijdens zijn presentatie zal hij onder meer vertellen over het onderzoek bij Vives en bij Inagro (ook een Vlaamse BioBoost-partner), over automatisering in een insectenkwekerij en over het gebruik van tuinbouwreststromen in de insectenkweek.

In het Rotterdamse BlueCity is De Krekerij gevestigd. Dit bedrijf maakt lekkere, voedzame en verantwoorde producten op basis van krekels en sprinkhanen. Directeur Sander Peltenburg zal tijdens het mini-symposium een korte pitch geven over De Krekerij. En vanzelfsprekend mogen deelnemers aan de bijeenkomst de krekelsnacks proeven.
