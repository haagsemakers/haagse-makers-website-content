---
type: event
path: /event/science-or-photography-design-or-fine-art
title: Science or Photography? Design or Fine Art?
date: 2019-10-08
tags: ["art"]
start: 2019-10-15 20:00:00
end: 2019-10-15 23:00:00
event_url: https://www.facebook.com/events/503588857088762/
organizers: ["zefir7", "stroom_den_haag", "kabk_photography"]
venue: het_koorenhuis
featuredImage: 72132763_1267113006783255_7215214525186834432_o.jpg
---
Three alumni KABK Photography tell about their science-focussed projects and publications.

Marijn van der Leeuw: “Meanwhile a solid rock can become incoherent sand and then a castle”
Louisiana van Onna: “Here we weigh more, there we jump higher”
Suzette Bousema: “Climate Archive”
(Not present but work shown: Sofie Sihombing: “The man for whom time went faster”)
