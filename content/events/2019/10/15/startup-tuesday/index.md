---
type: event
path: /event/startup-tuesday-151019
title: 'Startup Tuesday The Hague: World Startup Factory and The Hague Tech | Rinze de Vries'
date: 2019-10-04
tags: [ "startup" ]
start: 2019-10-15 17:00:00
end: 2019-10-15 19:00:00
event_url: https://www.startuptuesdaythehague.com/event-info/apollo14-ey-physee
organizers: ["startup_tuesday_the_hague", "world_startup_factory", "the-hague-tech"]
venue: 'the_hague_tech'
featuredImage: startup.png
---

Our speaker, Rinze likes to dive into new challenges and he is often eagerly in the search for new and better solutions for known problems. He was born in Friesland, our province with a lot of water, and therefore likes everything that has to do with this water; sailing, (kite)surfing, diving, swimming, and of course in the winter ice skating.

Together with Arnoud he is now building a company that wants to find sustainable solutions for complex problems we are facing in our world. Currently they are focussing on the problem of plastic in our rivers, but the lessons they are learning right now will be very valuable for new solutions that they want to develop later on.

_Startup Tuesday The Hague is an initiative of World Startup Factory, The Hague Tech, Apollo 14, YES!Delft The Hague and The Hague Humanity Hub, all proudly part of ImpactCity. And supported by, We Share Ventures and Hotelschool The Hague._
