---
type: event
path: /event/open-ateliers-2019
title: Open Ateliers 2019
date: 2019-10-03
tags: ["art"]
start: 2019-10-05 12:00:00
end: 2019-10-06 18:00:00
event_url: https://www.openateliersdenhaag.nl/
organizers: ["open_ateliers_den_haag"]
featuredImage: Jeanne-Figdor-1500x630.jpg
---

Open Ateliers is een jaarlijks kunstevenement in het eerste weekend van oktober. In dit weekend kunt u verschillende kunstenaars in de binnenstad van Den Haag in hun eigen atelier bezoeken. Laat u verrassen door het werk en geniet ook van de activiteiten die door verschillende kunstenaars worden aangeboden.

Wie weet komt u dit weekend eindelijk uw kunstwerk tegen!

De kunstenaars zijn zaterdag en zondag in hun atelier aanwezig om u te ontvangen!
