---
type: event
path: /event/startup-tuesday-081019
title: 'Startup Tuesday The Hague: Apollo14 | EY & Physee'
date: 2019-10-04
tags: [ "startup" ]
start: 2019-10-08 17:00:00
end: 2019-10-08 19:00:00
event_url: https://www.startuptuesdaythehague.com/event-info/apollo14-ey-physee
organizers: ["startup_tuesday_the_hague", "apollo14"]
venue: 'apollo14'
featuredImage: startup.png
---

Willem Kesteloo of Physee BV. shares their vision on the future-proof glass facade for next-generation sustainable buildings. Listen to how they do it and work together with EY in the process.

Startup Tuesday The Hague is an initiative of World Startup Factory, The Hague Tech, Apollo 14, YES!Delft The Hague and The Hague Humanity Hub, all proudly part of ImpactCity. And supported by, We Share Ventures and Hotelschool The Hague.
