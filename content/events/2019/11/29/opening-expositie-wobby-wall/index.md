---
type: event
path: /event/opening-expositie-wobby-wall
title: "Opening Expositie Wobby Wall"
tags:
  - grafisch
  - opening
start: 2019-11-29 20:00:00
end: 2019-11-29 23:00:00
event_url: http://grafischewerkplaats.nl/
venue: de_grafische_werkplaats
organizers:
  - grafische_werkplaats
featuredImage: 59886634-b1ab-48b6-aca0-fafcf5c61a3e.jpg

---
Hierbij nodigen we je uit voor het bezoeken van onze nieuwe expositie Wobby Wall en attenderen we je op het Other Book Festival.

De expositie Wobby Wall wordt tijdens Hoogtij op vrijdag 29 november om 20.00 uur geopend door Marjolein Schalk van Wobby en Carola van der Heijden van de Grafische Werkplaats.
Expositie Wobby Wall met werk van: Lisa Blaauwbroek, Bas de Geus, Jip Hilhorst, Jelmer Konjo, Alexandra Martens en Andrew Tseng

Ter gelegenheid van de eerste editie van The Other Book organiseren de Grafische Werkplaats samen met de makers van het magazine Wobby het Wobby Wall Project. Zes makers uit de regio Haaglanden die een bijdrage aan Wobby hebben geleverd of binnenkort een bijdrage leveren, nodigden we uit om nu niet alleen een design aan te leveren, maar ook om zelf achter de risograaf te staan en eventueel met andere grafische technieken te werken. Zo verschijnen er verschillende Wobby Walls in de werkplaats.

Expositie Wobby Wall
29 november- 7 februari 2020
De expositie is te zien tijdens het festival
en daarna op ma., wo., vr. van 10.00-17.00 u.
en op afspraak.
