---
type: event
path: /event/paagman-michiel-van-straten
title: Michiel van Straten bij Paagman
date: 2019-10-09
tags: ["books"]
start: 2019-11-01 19:00:00
end: 2019-11-01 21:00:00
event_url: https://www.paagman.nl/michiel-van-straten
organizers: ["paagman"]
venue: paagman_fred
featuredImage: michiel-van-straten-paagman.jpg
---
Op vrijdag 1 november is Michiel van Straten om 19:00 uur te gast bij Paagman, vestiging Fred (Frederik Hendriklaan 217, Den Haag). Hij zal die avond twee nieuwe boeken presenteren: 100 maritieme uitvindingen en 42 (onvergetelijke) vader & zoon-activiteiten. Aansluitend zal hij signeren.

100 maritieme uitvindingen en 42 (onvergetelijke) vader & zoon-activiteiten
Wat hebben maritieme uitvindingen en vader&zoon-activiteiten met elkaar te maken? Je kunt er boeken over schrijven. En dat heb ik gedaan. Het resultaat is de uitgave van twee boeken: 42 (onvergetelijke) vader&zoon-activiteiten en 100 Maritieme Uitvindingen. Bij twee verschillende uitgevers – dat leg ik tijdens de dubbelboekpresentatie wel uit. Ik vond het heel leuk om de boeken te schrijven. Ik heb er ook een hoop van opgestoken. Wist je bijvoorbeeld...
... dat de Pizza Marinara een maritieme uitvinding is?
... dat het maken van een Pizza een uitstekende vader&zoon-activiteit is?
... dat er een scheepscomputer is gevonden van tweeduizend jaar oud?
... dat het boek 42 (onvergetelijke) vader&zoon-activiteiten nou juist bedoeld is om vaders en zonen achter hun computers vandaan te halen?
... dat de Poolster een rol speelt in beide boeken?
... dat je nog veel meer te weten kan komen over 42 (onvergetelijke) vader&zoon-activiteiten en 100 maritieme uitvindingen als je naar de dubbelboekpresentatie komt? (Bijvoorbeeld of de 42 (onvergetelijke) vader&zoon-activiteiten ook geschikt zijn als moeder&dochter-activiteiten.)

Michiel van Straten
Michiel van Straten is freelance ontdekkingsschrijver en gediplomeerd schrijfdocent. Hij heeft meerdere artikelen voor diverse publiekstijdschriften, zoals Quest en National Geographic Junior, geschreven. In zijn boeken laat hij zien dat wat we vanzelfsprekend vinden vaak helemaal niet zo vanzelfsprekend is, of zelfs ronduit helemaal niet klopt. Eerdere boeken van Michiel van Straten zijn Onzeker op zee, waarin hij afrekent met op humoristische wijze met zeilverhalen uit de categorie “kijk mij eens” en Tien verdwenen dagen, waarin hij dergelijke alledaagse dingen onderzoekt, en daardoor verrassende verhalen ontdekt.

Tip: geniet voor het evenement van een avondmaaltijd in ons Kicking Horse Café. Iedere doordeweekse avond tussen 17:00 - 20:00 uur kunt u genieten van een heerlijke warme maaltijd voor slechts €14,50. Bekijk hier het wisselende weekmenu!
