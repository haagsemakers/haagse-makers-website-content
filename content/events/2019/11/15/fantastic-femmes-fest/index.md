---
type: event
path: /event/fantastic-femmes-fest
title: "Fantastic Femmes Fest"
date: 2019-11-14T23:00:00Z
tags:
- creativiteit
- ondernemerschap
- vrouwen
start: 2019-11-15 19:00:00
end: 2019-11-16 17:00:00
event_url: "https://fantasticfemmes.nl/"
venue: "het_koorenhuis"
featuredImage: fantastic.jpg

---
Celebrating female creativity & entrepreneurship
Op dit cultureel festival staan creatieve vrouwen in de spotlights. Verwacht twee dagen vol waanzinnige optredens, informatieve talks en inspirerende workshops.

## Where Fantastic Femmes meet
Fantastic Femmes Fest is het enige evenement in Den Haag dat zich richt op vrouwelijke artiesten en entrepreneurs uit de creatieve industrie. Het festival is een ontmoetingsplek waar performers, designers, schrijvers, vloggers en andere makers zich presenteren aan een breed publiek. Alle genders zijn welkom!

Op 15 en 16 november 2019 kun je genieten van twee avonden muziek, dans en poetry, terwijl overdag lezingen, panelsessies en workshops worden gegeven door ervaren en enthousiaste ondernemers. Doe mee, leer nieuwe skills en maak onderdeel uit van de groeiende #fantasticfemmes community.
