---
type: event
path: /event/the-hague-hacks-festival-2019
title: The Hague Hacks Festival 2019
date: 2019-10-01
tags: ["tech"]
start: 2019-12-06 10:00:00
end: 2019-12-06 22:00:00
event_url: https://thehaguepeace.org/haguehacks/festival/
organizers: ["the_hague_hacks", "the_hague_humanity_hub"]
venue: the_hague_humanity_hub
featuredImage: poster2019-pic1.jpg
---

Date: December 6th 2019
Place: Humanity Hub, The Hague
Price: Regular: € 15,- / Student € 5,-


The theme for this year’s festival is ‘Set Free: Tech to Empower Human Rights and Freedoms’. Rights and freedoms that you and I may take for granted are not self-evident in many states and communities. Today’s human rights violations will be illustrated by case studies from diverse countries and hosted in five special workshops, each dedicated to defending specific article(s) from the Universal Declaration of Human Rights.

These workshops connect grass-roots problem owners and human rights defenders with a diverse audience of cross-sector expertise to investigate and understand these challenges, and explore their contexts and mechanisms with a plurality of perspectives that enriches the problem-solving process and leads to innovative new tech ideas for human rights advocacy.

This year we are also dedicating a workshop to exploring methods and new technologies that could impact on global ecological issues. Our approach relies on the premise that we can actually solve the world’s problems togther, so please do turn up, we’d love to see you there! Until December 6th 🙂
