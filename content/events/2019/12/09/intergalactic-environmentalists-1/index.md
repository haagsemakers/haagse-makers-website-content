---
type: event
path: /event/intergalactic-environmentalists-1
title: "Intergalactic Environmentalists meet-up #1"
tags:
  - Food
  - Art
start: 2019-12-09 19:00:00
end: 2019-12-09 21:00:00
event_url: https://www.facebook.com/events/1050555701956691/
venue: panderbar
featuredImage: 73388567_122075769216673_1894898631366934528_o.jpg
organizers:
  - intergalactic_nvironmentalists
---

Join us, (Jonne Verhoog)[https://www.facebook.com/jonne.verhoog] and (Jorick de Quaasteniet)[https://www.facebook.com/jorick.dequaasteniet], for an evening of Intergalactic Environmentalism, sharing ideas and soup!

We are Intergalactic Environmentalists, and we want to encourage a more sustainable usage of our planet and outer space.

We want to do this by organizing exhibitions, events and meet-ups.
Our very first meet-up will take place on the 9th of December, in the Panderbar (Binnendoor 34, The Hague). From 19:00 until 21:00.

We will provide you with freshly cooked soup and a small presentation on Intergalactic Environmentalists. After this there will be a conversation about the current state of environmentalism, both on a planetary and individual scale.

See you there!

ps: There will be some beers, wine and tea, OP=OP
