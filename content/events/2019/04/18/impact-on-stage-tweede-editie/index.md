---
type: event
path: /event/impact-on-stage-tweede-editie
title: 'Impact on Stage: Tweede editie'
date: 2019-10-01
tags: ["Events","impact","social club den haag"]
start: 2019-04-18 17:30:00 
end: 2019-04-18 20:30:00 
event_url: https://www.eventbrite.co.uk/e/impact-on-stage-18-april-2019-tickets-56100240329?aff=ebdssbeac 
organizers: [null]
featuredImage: com-images-56408026-190118462188-1-original.jpg
---

Na een succesvolle editie op 9 oktober en op 17 januari 2019 vindt de volgende editie van Impact on Stage plaats op donderdag 18 april, een reeks inspiratiesessies waarbij elke keer 2 maatschappelijk bevlogen ondernemers spreken over een specifiek onderwerp. Tijdens de tweede editie van Impact on Stage gooien we het over de 'next-step'-boeg en we hebben hiervoor 2 hele vernieuwende bedrijven gevonden die hun verhaal over o.a. hun businessmodel met ons willen delen: Secrid en Mud Jeans! [Koop hier snel jouw kaartjes- de vorige keer zat de zaal bomvol!](https://www.eventbrite.co.uk/e/impact-on-stage-18-april-2019-tickets-56100240329?aff=ebdssbeac) **Secrid - René van Geer & Marianne van Sasse van IJssel** Secrid is een jong en ambitieus Haags bedrijf opgericht door ontwerpers René van Geer en Marianne van Sasse van IJsselt. Begin 2018 zijn zij verhuisd naar een gerenoveerde loods in de Binckhorsthaven en gaven daarmee een extra impuls om dit voormalige industrieterrein nieuw leven in te blazen. René en Marianne willen graag hun nieuwe hoofdkantoor laten zien en vertellen over Secrid, de maatschappelijke impact die zij nastreven en hoe zij door middel van inrichting en kunst een speciale werkplek creëren voor hun medewerkers. **Mud Jeans - Bert van Sun** Op 23 jarige leeftijd ging Bert naar China om te werken in de textielindustrie. Na 30 jaar in deze industrie heeft Bert de maatschappelijke gevolgen en de effecten op het milieu gezien. In 2013 richtte Bert Lease a Jean op om de impact van de jeansindustrie te verminderen. Met Mud Jeans probeert Bert mensen schuld vrij mooie jeans te laten dragen. Bert zal een inkijkje geven in hoe je een alternatief verdienmodel kan opzetten in de traditionele maakindsutrie. Social Club Leden krijgen veel voordeel: Social Club leden kunnen gratis naar binnen en niet-leden betalen € 15,00. Geen geld vinden wij voor zo’n inspirerend event inclusief een lichte maaltijd, drankjes en gelijkgestemden! Wees er snel bij, want er is een beperkt aantal plekken…