---
type: event
path: /event/kamera-kultura-circulariteit-en-de-europese-parlementsverkiezingen
title: 'Kamera Kultura: circulariteit en de Europese Parlementsverkiezingen'
date: 2019-10-01
tags: 
  - Circulair
  - Nutshuis
  - Talkshow
start: 2019-04-28 11:00:00 
end: 2019-04-28 13:00:00 
event_url: https://nutshuis.nl/events/kamera-kultura-28-april-2019/ 
organizers: [null]
featuredImage: rik-maarsen.jpg
---

Vertrouwd én verrassend: dat is ons maandelijkse programma Kamera Kultura. Jellie Brouwer (radiopresentator NTR Kunststof) ontvangt uiteenlopende gasten en Juni Café maakt de beste brunches. Eind mei vinden de Europese Parlementsverkiezingen plaats. Welke invloed hebben die verkiezingen op ons als Europeanen? Wat is het belang ervan? Jellie zal deze vragen stellen aan iemand die er als geen ander raad mee weet: **Kati Piri**. Sinds 1 juli 2014 is zij namens de Partij van de Arbeid lid van het Europees Parlement. De Nederlandse politica van Hongaarse afkomst wordt geprezen voor haar werk op het gebied van migratievraagstukken en haar invloed als Turkije-rapporteur. Het leverde haar twee jaar geleden de elfde plaats op in de lijst van belangrijkste Europarlementariërs, samengesteld door de Brusselse krant _Politico_. **Rik Maarsen** noemt zichzelf ‘circular designer’. De ontwerper, die vorig jaar afstudeerde aan de Design Academy Eindhoven, toont zijn maatschappelijke betrokkenheid in uiteenlopende projecten. Eén van die projecten is _Landscaped Furniture_: meubels die zijn gemaakt van Nederlands landbouwafval. Aan het einde van zijn levensduur kan het meubel dienen als voeding voor de grond waar het zijn oorsprong vond. Op die manier raakt het landschap niet uitgeput voor productie, maar blijven we ervoor zorgen. Jellie gaat met Rik in gesprek over zijn werk en ambities. **Joost Oomen** is dichter, schrijver, performer, muzikant en programmamaker. De veelkunner publiceerde twee poëziebundels: _Vliegenierswonden_ en _De stort_. Zijn novelle _[De zon als hij valt](https://shop.denieuweoost.nl/product/zon-als-valt-joost-oomen/)_, over de reis die een jongen, een meisje, een oog en een pols afleggen om samen te zijn, verschijnt dit jaar ook in Italiaanse vertaling. Joost is een begenadigd performer. Dat heeft hij eerder laten zien en horen op festival Crossing Border en Lowlands. Nu is het de beurt aan Kamera Kultura, waar Joost een column van eigen hand voordraagt. Hoog tijd dat het Franse chanson klinkt in Kamera Kultura! De rust van de zondag, de lentekriebels in april, de intimiteit van het Juni Café en het uitgebreide verse buffet: alles leent zich voor meeslepende melancholie en romantische taal. Laat dat maar over aan zangeres Wendy Maria Dekker en pianist Anton Titsing. Onder de titel **Chansons d’ Anton & Wendy** speelt het muzikale duo grootse nummers in minimalistische uitvoering. Van Brel tot Piaf, zomaar op een zondagmorgen.