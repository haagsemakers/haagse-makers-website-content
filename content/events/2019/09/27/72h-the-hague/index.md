---
path: '/event/72hrs-the-hague-film-contest-3'
title: 72hrs The Hague Film Contest
description: Op vrijdag 27 september 2019 om 13:00 uur wordt op feestelijke wijze de 3e editie van de 72hrs The Hague Film Contest afgetrapt! Ben je filmmaker, acteur, make-up artist, scriptschrijver, camera operator, video editor, gaffer, sound designer of regisseur? Dan is dit iets voor jou!
start: 2019-09-27 15:00:00
end: 2019-09-30 15:00:00
organizers: [ 'evenblij_film' ]
event_url: http://72hrs.nl
featuredImage: ../../images/events/72hrs_film_contest_2019_3000px.jpg
---
De 72hrs The Hague Film Contest is een initiatief van Evenblij Film en is gestart in 2017. Wij zijn er waanzinnig trots op om in 2019, voor de derde keer op rij dit Haagse filmevenement in samenwerking met de Gemeente Den Haag te organiseren.
