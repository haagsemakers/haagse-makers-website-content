---
path: /event/parking-day-2019
title: Park[ing] Day 2019
description: Park[ing] Day is een internationaal fenomeen, een jaarlijks terugkerende interventie waarbij wereldwijd bewoners, initiatieven, creatieven, ontwerpers en kunstenaars, voor één dag parkeerplekken ombouwen tot kleine publieke parken.
featuredImage: ../../images/events/parkingday_2019.png
start: 2019-09-21 13:00:00
end: 2019-09-21 19:00:00
venue: Binnenstad Den Haag
organizers: [ Parking Day ]
event_url: https://parkingdaydenhaag.nl/
---
Geef je op, bedenk een plan en rol je mat uit op vrijdag 20 september. De route loopt in 2019 door het Oude Centrum van Den Haag: Boekhorststraat, Gedempte Burgwal, Paviljoensgracht en niet te vergeten de Bierkaden. Dit jaar maken we de parkeerplaatsen niet alleen groen, maar ook blauw!
We hebben namelijk heel veel reacties gekregen van mensen die meer water in de stad willen. Water werkt het beste tegen hittestress, het is goed voor de opvang van hoosbuien en bovendien ziet het er ook nog eens aangenaam uit.
Wil je ook een parkeerplaats claimen voor 1 dag, om jouw idee te laten zien?
