---
type: event
path: /event/fred-match-maker
title: 'Fred Match Maker'
date: 2019-10-01
start: 2019-05-17 17:00:00 
end: 2019-05-17 23:59:00 
event_url: http://www.youngthehague.nl/tc-events/fred-match-maker/ 
organizers: [null]
featuredImage: fred-match-maker-1140x641.png
---

**Fred Match Maker** Date of event: 17-05-2019 Time start-end: 17:00 – 00:00 Date closing of registration: No need to register Locations of the event: HofHouse Language of event:  Nederlands & Engels Spots available: unlimited Event main category: Fred Match Maker 2019 **Over de Fredborrel:** We zijn weer terug met een Fredborrel tijdens de MatchMaand, ditmaal in HofHouse! Volledig in het thema ‘kruisbestuiving’ hebben we voor jullie weer een match-maker element in de borrel verwerkt. Verwacht 17 mei hapjes, drankjes, muziek en een hoop gelijkgestemde young professionals! En dat voor gratis entree! Houd vooral de [facebookpagina](https://www.facebook.com/events/2276394049063525/) in de gaten! ///ENGLISH/// We’re back with Fred during the MatchMonth this time at HofHouse! Totally in line with this month’s theme, we again included a match-maker element in this Friday-evening-drinks-event. On the 17th of May you can expect bites, drinks, music and a lot of likeminded young professionals. All this – without entrance fee! Keep a close eye on [our facebookpage](https://www.facebook.com/events/2276394049063525/)! **Overview of the program:** 17:00 doors open, party starts! 00:00 doors close & event ends **Practical information:** HofHouse ligt dicht bij Centraal station Den Haag. Belangrijk is om je ID mee te nemen als je alcoholische dranken wilt bestellen. Makkelijkst is om op de fiets te komen of met OV. HofHouse is very close to The Hague Central Station. Its important to bring your ID if you want to consume alcoholic beverages. It is easiest to come by bike or public transport.