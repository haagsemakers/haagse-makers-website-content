---
type: event
path: /event/workshop-samen-naar-een-circulaire-deeleconomie
title: 'Workshop: Samen naar een circulaire deeleconomie'
date: 2019-10-01
tags: ["circulaire economie","deeleconomie","sharenl"]
start: 2019-05-21 13:45:00 
end: 2019-05-21 18:00:00 
event_url: https://www.collaborativexcircular.com/thehague 
organizers: [null,null]
featuredImage: denhaag-3-2ratio.jpg
---

Woon of werk je in Den Haag en ben je actief met delen of duurzaamheid? Of ken je lokale organisaties die actief zijn in de deel- of circulaire economie? Dan hebben we jou nodig! Meld je hierboven aan…, of lees hieronder eerst meer over het project.

We hebben jou nodig omdat we aan iets bijzonders werken. In heel Europa wordt er door burgers zoals jij gewerkt aan nieuwe vormen van economie die bijdragen aan een duurzame en sociale leefomgeving. De deeleconomie verbindt mensen rechtstreeks met elkaar, zodat ze allerlei goederen kunnen delen, lenen, ruilen en verhuren. Denk aan het Haagse Camptoo, een online platform waar mensen elkaars ongebruikte campers huren of Krijgdekleertjes waar kinderkleding geruild wordt. De circulaire economie biedt nieuwe bedrijfsmodellen waarbij het afval van de ene persoon of organisatie de hulpbron van een ander is. De Stadswerkplaats Den Haag en de Haagse stadszagerij zijn mooie voorbeelden van een lokale circulaire economie. Neem je de deeleconomie en de circulaire economie samen, dan biedt dat unieke kansen voor onze steden, de maatschappij, het bedrijfsleven en het milieu.

Den Haag werkt samen met steden in Vlaanderen, Portugal, Italië, Slovenië en Griekenland om de kansen van een circulaire deeleconomie optimaal te benutten. Het doel van dit project is om de inwoners, ondernemers, initiatiefnemers en stadsbesturen van deze steden in staat te stellen om van elkaar te leren maar ook om voor Europese en lokale overheden te verkennen hoe zij de circulaire deeleconomie kunnen faciliteren. Ben jij actief in de deel- en/of circulaire economie, en wil jij de impact van jouw organisatie vergroten door kennis en ervaringen te delen met stadgenoten binnen en buiten Den Haag? Meld je dan nu aan!

**Extra belangrijk voor Den Haag: Armoedebestrijding & sociaal ondernemerschap**

Alle deelnemende steden kijken naar de lokale circulaire deeleconomie als geheel maar leggen daarbij extra focus op specifieke stedelijke uitdagingen. Voor de stad Den Haag zijn sociaal ondernemerschap en armoedebestrijding van belang. Dus draagt jou organisatie bij aan armoede bestrijding en/of ben je een sociaal ondernemer in de circulaire of de deeleconomie. Meld je aan!

**Waarom meedoen?**

Door deelname aan dit project:

*   kun je leren van (vergelijkbare) initiatieven en organisaties uit Den Haag en andere Europese steden;
    
*   help je organisaties en initiatiefnemers in diverse landen verder;
    
*   kom je direct in contact met de beleidsmakers;
    
*   wordt de positieve impact die je probeert te hebben versterkt!
    

**Wat doen we met jouw inbreng?**

We doen onze uiterste best om de ervaringen en ideeën van jou en vele andere in de diverse Europese steden, zo bruikbaar mogelijk zichtbaar te maken voor jou, de mensen en organisaties in jouw stad, en diverse stedelijke en Europese overheden. Hierbij blijf jij anoniem tenzij je nadrukkelijk toestemming hebt gegeven om persoonsgebonden informatie te delen. Door van elkaar te leren hoeft niet overal het wiel (volledig) opnieuw uitgevonden te worden en kunnen de Haagse buurten en steden uit heel Europa sneller de kansen van de circulaire deeleconomie to volle wasdom laten komen.

Meer lezen over dit project? [Klik hier](https://www.collaborativexcircular.com/about-1) \[Engelstalige pagina\]