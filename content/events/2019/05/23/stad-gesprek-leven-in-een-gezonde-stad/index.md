---
type: event
path: /event/stad-gesprek-leven-in-een-gezonde-stad
title: 'STAD gesprek: Leven in een gezonde stad'
date: 2019-10-01
tags: ["gezondheid","platform stad","stad"]
start: 2019-05-23 16:00:00 
end: 2019-05-23 18:00:00 
event_url: https://www.platformstad.nl/agenda/leven-in-een-gezonde-stad/ 
organizers: [null]
featuredImage: gezonde-stad-platform-stad-o5qa8tn22rhsmu8j7r7t4hwj5xwbh4icmkzj2a8heo.jpg
---

**Wereldwijd maken steden op dit moment een ongekende groei door. Als steden op de langere termijn aantrekkelijk en vitaal willen blijven dan moeten ze de gezondheid van hun bewoners in hun beleid voorop stellen.** Want gezonde steden zijn aantrekkelijke steden. Steden, als Kopenhagen en Zurich, die de WHO-richtlijnen op het gebied van Healthy Urban Planning (HUP) meenemen in hun beleid staan hoog genoteerd in de internationale top tien van aantrekkelijke vestigingsplekken. Belangrijkste richtlijn is dat de stad zo ingericht is dat bewoners en gebruikers geprikkeld worden om meer te bewegen en elkaar te ontmoeten. Schone mobiliteit en de aantrekkingskracht van groen spelen hierbij een belangrijke rol. **Kan ruimtelijk ontwerp gezond gedrag beïnvloeden? ** Wat is er nodig om bij de groei van Den Haag een gezonde en aantrekkelijke leefomgeving te waarborgen die bewoners uitnodigt tot meer bewegen en een gezonde leefstijl? Kan ruimtelijk ontwerp gezond gedrag beïnvloeden? En hoe zorgen andere steden voor een gezonde en aantrekkelijke leefomgeving? De gemeente Utrecht bijvoorbeeld stelt de gezondheid van haar inwoners centraal in haar beleid. Leidt dat bij nieuwe ontwikkelingen, zoals Smakkelaarsveld, tot andere keuzes? Platform STAD gaat in gesprek met oa: Hans Jansen (adviseur Leefomgeving, GGD Haaglanden), Wendy van Kessel (Urhahn Stedenbouw) en Ellen Peeters (adviseur gezonde Leefomgeving, gemeente Utrecht). [Meer informatie en aanmelden](https://www.platformstad.nl/agenda/leven-in-een-gezonde-stad/)