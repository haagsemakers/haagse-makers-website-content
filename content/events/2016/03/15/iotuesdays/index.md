---
type: event
path: /event/iotuesdays
title: 'IoTuesdays'
date: 2019-10-01
tags: ["Events","Haagse Makers","IoT","IoTuesdays"]
start: 2016-03-15 17:00:00 
end: 2016-03-15 18:30:00 
organizers: ["haagse_makers"]
locations: ["icx"]
---

IoTuesdays: een middag/avond waar makers samenkomen, en samenwerken aan IoT projecten. Wat ga jij maken? Projecten waar nu aan gewerkt wordt: • IoT slim slot • [DIY lora gateway](https://community.haagsemakers.nl/t/iotuesday-project-diy-lora-gateway/82) • Experimenteren met lora sensoren • Leren over nieuwe technologie die wordt gedeeld door het Permanent Future Lab • Vorige meetup: [http://www.meetup.com/haagsemakers/events/229037722/](http://www.meetup.com/haagsemakers/events/229037722/) -- Achtergrondinformatie Iedere twee weken is het IoTuesday. Deze keer richten we ons op het werken aan projecten. Volgende keer zijn er weer presentaties, waar mensen over hun IoT project vertellen en ervaringen worden uitgedeeld. Je bent welkom!