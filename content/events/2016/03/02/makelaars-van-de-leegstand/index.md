---
type: event
path: /event/makelaars-van-de-leegstand
title: 'Makelaars van de Leegstand'
date: 2019-10-01
tags: ["culturele business case","Events","leegstand","schouwburg","theater"]
start: 2016-03-02 17:00:00 
end: 2016-03-02 19:00:00 
event_url: http://culturelebusinesscase.nl/business-cases/makelaars-van-de-leegstand-in-de-ks/ 
organizers: ["koninklijke_schouwburg_den_haag"]
locations: ["koninklijke_schouwburg_den_haag"]
featuredImage: ks.jpg
---

De Koninklijke Schouwburg heeft geregeld fysieke ruimtes vrij. Makers zonder vaste ruimte hebben behoefte aan deze ruimte. En beide willen zij publiek trekken en elkaars krachten – frisse creatie – en – een pand met allure- kunnen elkaar enorm versterken. 1+1=3. **Concept** De Makelaars van de Leegstand is te vormen een collectief van (creatieve-) ondernemers die op korte termijn invulling kunnen geven aan avonden (middagen) met ruimte in de programmering bij de KS. Op **2 maart 2016 om 17uur** organiseert de KS daarom een eerste kennismaking van deze ondernemers met de KS. Elke Smet van de KS geeft je een rondleiding door de schouwburg en beantwoord zoveel mogelijk van je vragen. Enkele uitgangspunten:

*   Met jouw unieke evenement trek jij nieuw publiek;
*   Dit evenement past binnen het imago en activiteiten van de Koninklijke Schouwburg;
*   Het evenement heeft een ludiek en eenmalig karakter, dat duidelijk buiten reguliere programmering of verhuringsevenementen van de schouwburg valt;
*   Het evenement wordt met de Koninklijke Schouwburg afgestemd;
*   Het evenement moet voor de KS kostendekkend zijn;
*   Jij als initiatiefnemers en/of producent mag uiteraard een commercieel belang nastreven.