---
type: event
path: /event/analog-borders-festival
title: 'Analog borders festival'
date: 2019-10-01
tags: ["Events","grafische werkplaats","Workshops"]
start: 2016-05-20 08:00:00 
end: 2016-06-26 17:00:00 
event_url: grafischewerkplaats.nl 
organizers: ["grafische_werkplaats"]
locations: ["de_grafische_werkplaats"]
featuredImage: beeld-analog-borders-klein.jpg
---

Festival Analog Borders Analog Borders is een festival voor kunstenaars, designers en andere doeners in de Grafische Werkplaats. Met een expositie, masterclasses, workshops en een symposium. Verken de grenzen van de traditionele grafische technieken, experimenteer met en verdiep je in nieuwe mogelijkheden! Mix analoge met digitale technieken, gebruik huis-tuin-en-keukenmaterialen of combineer totaal verschillende technieken met elkaar. Ets, gomdruk, analoge en digitale fotografie, riso, zeefdruk: net even anders!