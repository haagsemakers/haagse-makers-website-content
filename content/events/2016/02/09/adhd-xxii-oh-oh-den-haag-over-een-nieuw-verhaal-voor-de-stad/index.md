---
type: event
path: /event/adhd-xxii-oh-oh-den-haag-over-een-nieuw-verhaal-voor-de-stad
title: 'ADHD XXII: Oh Oh Den Haag'
date: 2019-10-01
tags: ["debat","Events"]
start: 2016-02-09 20:00:00 
end: 2016-02-09 22:00:00 
event_url: http://stroom.nl/activiteiten/reserveren.php?a=ADHD+XXII%3A+Oh+Oh+Den+Haag&lang=nl 
organizers: ["stroom_den_haag","bna_haaglanden","hhac","het_nutshuis","platform_stad"]
locations: ["nutshuis"]
featuredImage: 25459-adhd-logo.jpg
---

ADHD XXII: Over een (nieuw) verhaal voor de stad
------------------------------------------------

_ADHD legt haar oor te luisteren bij diverse ‘verhalenvertellers'. ‘All good narrative has the property of exploring the unforeseen, of discovery; the novelist's art is to shape the process of exploration'_, aldus de Amerikaanse socioloog Richard Sennett. Volgens hem zijn verhalen een veel geschikter hulpmiddel om ontwikkelingen te duiden dan ontwerpen of visies. Die opvatting wordt steeds vaker gedeeld, maar hoe kom je tot een goed verhaal? Hoe wordt een verhaal geloofwaardig en ‘van iedereen'? Tijdens het congres [_Ruimte voor de stad_](http://www.ruimtevoordestad.nl) in september 2015 werd aan de zaal gevraagd: wat is het verhaal van Den Haag? Op het antwoord ‘internationale stad van vrede en recht - maar dat is niet iets van ons' na, bleef het stil. Is er echt geen breed gedragen verhaal van Den Haag? Of is het er niet één, maar zijn het er wel tien? Geograaf **Kees Terlouw** van de Universiteit Utrecht gaat in op het belang van verhalen voor de identiteit van de stad. Terlouw deed dit jaar onderzoek voor het Ministerie van Binnenlandse zaken naar gebruik, slijtage en vernieuwing van lokale identiteiten. Schrijver [**Marc Fabels**](http://marcfabels.com/log/) neemt verschillende Haagse verhalen onder de loep en maakt er een nieuwe versie van. Kunstenaar [**Onno Dirker**](http://www.dirker.nl) roept ‘de muzen' aan voor een nieuw verhaal over het centrum van Den Haag. En stedenbouwkundige [**Boris Hocks**](http://posad.nl/bureau/boris-hocks/) (Posad) vertelt een verhaal vanuit de ruimtelijke identiteit van de stad. **Jan Paternotte** gaat in gesprek met hen en de zaal over het Haagse verhaal.

ADHD
----

ADHD is een samenwerking tussen [BNA Kring Haaglanden](http://bnahaaglanden.nl), [Haags Architectuur Café](http://www.haac.nu), [Het Nutshuis](https://www.nutshuis.nl/), [Stroom Den Haag](../../) en [Platform Stad.](http://www.platformstad.nl) **[Stroom's ADHD Archief](../../paginas/pagina.php?pa_id=962532)** Overzicht alle ADHD avonden op de Stroom website.

dinsdag 09 feb '16 20:00 - 22:00 uur

Het Nutshuis, Riviervismarkt 5, Den Haag

Entree: gratis

Reserveren is noodzakelijk:

[Reserveringsformulier](/activiteiten/reserveren.php?a=ADHD+XXII%3A+Oh+Oh+Den+Haag&lang=nl)