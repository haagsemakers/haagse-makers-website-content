---
type: article
path: /haagsemakers-reset
date: 2019-10-01
title: Nieuwe start voor Platform Haagse Makers
excerpt: Na een rustige periode wordt Haagse Makers verder ontwikkeld tot het platform voor de Haagse Do-It-Together beweging.
author: arn_van_der_pluijm
featuredImage: athlete-body-cinder-track-4078.jpg
sections: 
  - Haagse Makers
---

Haagse Makers is in 2014 ontstaan vanuit de eerste Haagse Makersbeurs. Vanuit het idee om de Haagse Willie Wortels bij elkaar te brengen en op een publiek podium te laten zien. In de jaren daarna zijn een aantal richtingen en projecten geprobeerd, met de ambitie om een platform voor de Haagse Maker Movement te zijn. Afgelopen jaar lag het even stil,maar er wordt nu met hernieuwde energie gewerkt om het platform verder te versterken. 

## De toekomstplannen
Tot nu toe was Haagse Makers een losse organisatievorm waarbij er werd gewerkt aan projecten en bijeenkomsten. Op zich werkte dit goed, maar er blijkt toch wat meer vraag naar structuur. Daarom is besloten om van Haagse Makers een officiele stichting te maken: Stichting Platform Haagse Makers.

Deze stichting heeft als doelstelling om de Haagse Do-It-Together mentaliteit en community in de stad Den Haag te verterken. In de projecten en ontwikkelingen van de afgelopen jaren is naar voren gekomen dat deze do-it-together mentaliteit in de stad nog een stuk sterker kan zijn.

En waarom zou je dat willen? Met Haagse Makers geloven we erin dat een maatschappij waarin do-it-together een belangrijke factor is een betere en fijnere stad oplevert. Een stad waar iedereen het kan maken!

## Uitvoering
Om dit te bereiken is er heel veel wat je kan gaan doen. We hebben de plannen nu ingedeeld in 4 onderdelen:

  * **Communicatie**: We willen via de website haagsemakers.nl een inspiratiebron zijn voor iedere (nieuwe) maker in de stad. Door artikelen over relevante ontwikkelingen (in de stad en daarbuiten), door het bijhouden van de makersagenda van de stad, en door het aanbieden van een [Community Website](https://community.haagsemakers.nl).
  * **Maakplaatsen en workshops**: Voor een stad waar iedere maker zich kan ontplooien zijn (open) maakplaatsen essentieel. In Den Haag zijn er (nog steeds) niet zoveel van dit soort plekken. We gaan ernaar werken dat iedere maker toegang heeft tot dit soort maakplaatsen. En, net zo belangrijk, we gaan ervoor zorgen dat er een goed aanbod komt van workshops en lessen, zodat iedereen een echte maker kan worden.
  * **Programmareeksen**: We zien ruimte voor het opzetten van series bijeenkomsten rondom een bepaald thema. Inhoudelijke bijeenkomsten voor een breed publiek die op deze manier de do-it-together beweging in de stad kunnen versterken. Doordat ze kennis en inspiratie voor de maker opleveren, maar vooral ook doordat het een moment is waar de makers samenkomen.

## Website
Afgelopen periode is de website geupdate. De vorige website (gebaseerd op wordpress), is omgezet naar een website die makkelijker / flexibeler is om te beheren. Dit is het eerste artikel van 'de redactie'. De bedoeling is dat deze sectie in de komende tijd (jaren?) gaat uitgroeien tot een redactie waarin elke aspect van maken, vindingrijkheid en creatie wordt belicht.

## Meedoen?
De stichting en de nieuwe plannen worden op dit moment vormgegeven. Heb je interesse om bij te dragen aan Stichting Platform Haagse Makers? Neem dan vooral [contact]('/contact') op!
