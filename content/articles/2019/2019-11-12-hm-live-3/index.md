---
type: article
draft: true
path: /hm-live-3
date: 2019-11-12
title: "Haagse Makers Live! 3: Jeroen Boon van de Buurtschuur"
excerpt: Na een rustige periode wordt Haagse Makers verder ontwikkeld tot het platform voor de Haagse Do-It-Together beweging.
tags: 
  - Haagse Makers Live!
  - Talkshow
  - Buurtschuur
  - Jeroen Boon
  - Deeleconomie
sections: 
  - Live!
author: arn_van_der_pluijm
intro: "Haagse Makers is in 2014 ontstaan vanuit de eerste Haagse Makersbeurs. Vanuit het idee om de Haagse Willie Wortels bij elkaar te brengen en op een publiek podium te laten zien. In de jaren daarna zijn een aantal richtingen en projecten geprobeerd, met de ambitie om een platform voor de Haagse Maker Movement te zijn. Afgelopen jaar lag het even stil,maar er wordt nu met hernieuwde energie gewerkt om het platform verder te versterken. "
featuredImage: null
---