---
type: article
draft: true
path: /hm-live-1
date: 2019-11-10
title: "Haagse Makers Live #1: Sacha Winkel van Parking Day"
excerpt: Na een rustige periode wordt Haagse Makers verder ontwikkeld tot het platform voor de Haagse Do-It-Together beweging.
author: arn_van_der_pluijm
tags: 
  - Haagse Makers Live!
  - Talkshow
  - Parking Day
  - Buitenruimte
sections: 
  - Live!
featuredImage: img.jpg
---
Haagse Makers is in 2014 ontstaan vanuit de eerste Haagse Makersbeurs. Vanuit het idee om de Haagse Willie Wortels bij elkaar te brengen en op een publiek podium te laten zien. In de jaren daarna zijn een aantal richtingen en projecten geprobeerd, met de ambitie om een platform voor de Haagse Maker Movement te zijn. Afgelopen jaar lag het even stil,maar er wordt nu met hernieuwde energie gewerkt om het platform verder te versterken.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lTVbcQQOKrI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
