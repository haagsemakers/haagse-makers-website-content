---
type: article
draft: true
path: /hm-live-2
date: 2019-11-11
title: "Haagse Makers Live! #2: Mark van Duijn van Bokashi Den Haag"
excerpt: Na een rustige periode wordt Haagse Makers verder ontwikkeld tot het platform voor de Haagse Do-It-Together beweging.
author: arn_van_der_pluijm
tags: 
  - Haagse Makers Live!
  - Talkshow
  - Bokashi
  - Circulair
sections: 
  - Live!
featuredImage: null
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/R5fRCTKIydo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>