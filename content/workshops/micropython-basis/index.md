---
type: workshop
path: /workshops/micropython-basis
date: 2019-09-25
title: microPython introductie
excerpt: Leer de basis van het werken met microPython op een microcontroller voor jouw interactieve project.
intro: "Na het volgen van deze workshop kun je zelfstandig aan de slag met het programmeren van een microcontroller (ESP8266/32) met de programmeertaal microPython."
featuredImage: header_img.png
organizers: [ 'haagsemakers' ]
locations: [ 'het_koorenhuis' ]
openbadge_link: "https://badgr.io/issuer/issuers/ufWlxU94TBynX3jwQivfHA/badges/qOTCCRTyQZW6x6scCNtJLg"
events: [ 1 ]
---

### Over deze workshop
In deze workshop helpen we je op weg om te starten met microPython. Dit is een programmeertaal (een afgeleide van Python), waarmee je microcontrollers (zoals een ESP8266, ESP32 of Micro.Bit) kunt aansturen. De microcontroller is het brein van jouw project waar bijvoorbeeld interactie of beweging voor nodig is.
We hebben gemerkt dat het starten met het programmeren van microcontrollers ingewikkeld kan zijn. In deze workshop nemen we samen met jou deze eerste horde en zorgen we ervoor dat je zelfstandig aan de slag kan gaan met microPython!

### Waarom MicroPython
[MicroPython](https://micropython.org) is een afgeleide van [Python](https://python.org). Python is een ontwikkeltaal (ontwikkeld door een Nederlander!) die heel veel gebruikt wordt en ook steeds populairder wordt. Het meest bekend is het programmeren van microcontrollers met Arduino. Met de komst van betaalbare en krachtigere controllers zoals de ESP8266 en ESP32 is het nu ook mogelijk om met python te programmeren. Naast dat de taal eenvoudiger is dan Arduino / *C*, gaat het ontwikkelen ook sneller omdat de code direct op de microcontroller wordt uitgevoerd.
Er komen steeds meer tools en bibliotheken beschikbaar voor microPython. Je kan nu al zo goed als alles maken wat je zou willen.

### Wat ga je leren
Deze workshop van 1 dagdeel is voor de starter, die wil beginnen met microPython. We helpen je op weg en we zorgen ervoor dat iedereen aan het einde van de workshop aan de slag kan gaan. In de workshop komen de volgende onderwerpen aan de orde:

  * Installatie en korte uitleg van python en esptools (we ondersteunen recente versies van Windows en macOS).
  * Installatie en korte uitleg van Visual Studio of Atom. Dit zijn de code editors die we ondersteunen die je nodig hebt om te programmeren.
  * Hoe werkt REPL en hoe kun verifieren of de microPython interpreter werkt.
  * De werking van het bestadsysteem (Wat doet boot.py en main.py).
  * Zelf installeren en programmeren van een simpele toepassing in microPython.
  * Gezamenlijk realiseren van een eerste MicroPython-toepassing op een ESP8266 m.b.v de IDE.

Voor deze start-workshop is geen Pythonkennis of andere programmerkennis nodig, wel een goed stel hersens en inzet. Na de workshop heb je de basis om op eigen kracht zelf MicroPython-toepassingen te kunnen ontwikkelen.

### Ontwikkelboard
We hebben twee betaalbare ontwikkelboards geselecteerd op basis van de ESP32 microcontroller:
  - ESP32 met OLED Display ([Link](https://www.tinytronics.nl/shop/nl/display/oled/esp32-wi-fi-en-bluetooth-board-met-oled-display-cp2102))
  - ESP32 met OLED display en batterijhouder [Link](https://nl.aliexpress.com/item/32905330340.html)

Een ontwikkelbard zit inbegrepen in de workshop. Heb je al een board (en weet je zeker dat die werkt), of je wilt zelf bestellen dan kan dat natuurlijk ook. Om de workshop te volgen is een ontwikkelboard noodzakelijk.

## Voorkennis en wat neem je zelf mee
We gaan ervan uit dat je met computers kunt omgaan. Kennis van Python, MicroPython of een andere programmeertaal is **niet** nodig, maar natuurlijk wel handig. We verwachten wel een goed stel hersens en inzet.
Daarnaast is het noodzakelijk om je eigen laptop mee te nemen, waar je programma's op kunt installeren (je moet administrator zijn). Op de harde schijf moet ruimte zijn om additionele software te installeren, omgeveer 400Mb.

Na succesvol afronden van deze workshop ontvang je een certificaat van Haagse Makers. Met dit certificaat (gevalideerd via OpenBadges), kun je anderen je opgedane kennis laten zien.

## Aanmelden
De kosten voor deze MicroPython workshop (inclusief ontwikkelboard) zijn €22,50 per persoon (incl. BTW).
Naast de data die we zelf plannen en aanbieden is het ook mogelijk om deze workshop op andere data of locaties plaats te laten vinden. Neem daarvoor contact op [via academy@haagsemakers.nl](mailto:academy@haagsemakers.nl)

De volgende workshop staat gepland op dinsdag 4 februari van 18:30 tot ca 22:00 uur. Het aantal plaatsen is beperkt, dus meld je snel aan!
