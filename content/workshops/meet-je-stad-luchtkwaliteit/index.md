---
path: /workshops/meet-je-stad-luchtkwaliteit
date: 2019-08-11
title: Meet de luchtkwaliteit in je stad
excerpt: Bouw een fijnstof sensor en ga de luchtkwaliteit zelf meten.
intro: Bouw een eigen sensor waarmee je zelf het fijnstof in de lucht kunt gaan meten. Gebruik de kennis en ervaring van het OK Lab Stuttgart dat het Citizen Science project ‘luftdaten.info’ heeft  opgestart.
featuredImage: header_img.png
organizers: [ 'haagsemakers' ]
locations: [ 'het_koorenhuis', 'eigen_locatie']
objectives: [
  'Meet je stad',
  'Nog een objective'
]
---

## Meet je stad

Inmiddels meten 300 zelfgebouwde sensors in Stuttgart en ver daar buiten het fijnstof in de lucht. Daaruit genereert  Luftdaten.info in real time een kaart  waarop per locatie de gemeten  hoeveelheid  fijnstof zichtbaar is.
Ook in Nederland wordt met eigen sensors fijnstof in de lucht gemeten. Op een aantal plaatsen met sensors die volgens het concept van het OK Lab zijn gebouwd.

De gemeten waarden zijn zichtbaar op de [luftdaten.info fijnstofkaart](http://maps.luftdaten.info/#6/51.165/10.455) en op het [RIVM dataportaal](http://samenmeten.rivm.nl/dataportaal/).

Lees meer over [Citizen Science](https://www.samenmetenaanluchtkwaliteit.nl/citizen-science) voor het meten van de luchtkwaliteit in Nederland op het kennisportaal [samenmetenaanluchtkwaliteit.nl ](http://samenmetenaanluchtkwaliteit.nl/). Actuele waarden van de luchtkwaliteit in Nederland [luchtmeetnet.nl](http://luchtmeetnet.nl).

### Aan welke vragen wordt gewerkt?
* Hoe kun je de de luchtkwaliteit zelf meten en zichtbaar maken?
* Wat zijn de gevolgen van toenemende verkeer voor onze lucht?
* Hoe hoog is de belasting door fijnstof en stikstofoxiden in woonwijken langs de snelwegen en verder daar van af?  

Deze en andere vragen beantwoorden wij door fijnstof sensors te bouwen en de gemeten waarden in een totaal beeld weer te geven.

OK Lab Stuttgart-gegevens zijn beschikbaar onder de Database Contents License (DbCL) v1.0. De community mag en moet deze kaart verder ontwikkelen.


### Creative Technology
Deze workshop is onderdeel van de Creative Technology Hub Den Haag van Haagse Makers & Het Koorenhuis. Het doel van dit programma is om (onder andere) ontwikkelingen in transparantie, Open Data en Citizen Science te bevorderen.
Regionale groepen van ontwerpers, ontwikkelaars, journalisten en anderen ontmoeten elkaar regelmatig in labs. Ze ontwikkelen apps die de samenleving informeren, positief vorm geven en ondersteunen, en maken het werk van overheden en openbare instellingen transparanter.

Code a difference: Gebruik je vaardigheden om je stad, je dorp en je omgeving te verbeteren!

### Aanmelden en meer informatie
Deze workshop is geschikt voor groepen tussen de 5 en 10 personen. Er zijn op dit moment nog geen vaste date en/of prijzen. Wil je meer weten over beschikbaarheid en kosten? Neem [contact](/contact) op!
